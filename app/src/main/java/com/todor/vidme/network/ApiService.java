package com.todor.vidme.network;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

public class ApiService {

    private static final String BASE_URL = "https://api.vid.me";

    public API getApiService() {
        OkHttpClient httpClient = new OkHttpClient();
        httpClient.interceptors().add(new LoggingInterceptor());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(httpClient)
                .build();

        return retrofit.create(API.class);
    }
}
