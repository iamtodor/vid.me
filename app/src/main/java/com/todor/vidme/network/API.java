package com.todor.vidme.network;

import com.todor.vidme.model.LoginResponse;
import com.todor.vidme.model.VideoResponse;

import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;

public interface API {

    @GET("/videos/featured")
    Observable<VideoResponse> getFeaturedVideo(@Query("limit") int limit);

    @GET("/videos/featured")
    Observable<VideoResponse> getNextFeaturedVideo(@Query("limit") int limit,
                                                   @Query("offset") int offset);

    @GET("/videos/following")
    Observable<VideoResponse> getFollowedVideo(@Header("AccessToken")  String token,
                                               @Query("limit") int limit);

    @GET("/videos/following")
    Observable<VideoResponse> getNextFollowedVideo(@Header("AccessToken")  String token,
                                                   @Query("limit") int limit,
                                                   @Query("offset") int offset);

    @GET("/videos/new")
    Observable<VideoResponse> getNewVideo(@Query("limit") int limit);

    @GET("/videos/new")
    Observable<VideoResponse> getNewNextVideo(@Query("limit") int limit,
                                              @Query("offset") int offset);

    @POST("/auth/create")
    Observable<LoginResponse> login(@Query("username") String username,
                                    @Query("password") String password);

    @POST("/auth/delete")
    Observable<LoginResponse> logout(@Query("token") String token);
}
