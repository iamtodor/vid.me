package com.todor.vidme.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.todor.vidme.R;
import com.todor.vidme.interfaces.OnItemClickListener;
import com.todor.vidme.model.Video;
import com.todor.vidme.model.VideoResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoAdapterNew extends RecyclerView.Adapter<VideoAdapterNew.ViewHolderItem> {

    private Context mContext;
    private List<Video> mVideoList;
    private OnItemClickListener onItemClickListener;

    public VideoAdapterNew(Context mContext, VideoResponse videoContainer, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.mVideoList = videoContainer.getVideos();
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderItem(LayoutInflater.from(parent.getContext()).inflate(R.layout.video_item_new,
                parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolderItem holder, int position) {
        Video video = mVideoList.get(position);
        Glide.with(mContext).load(video.getThumbnailUrl()).into(holder.videoPreview);
        holder.videoName.setText(video.getTitle());
        holder.likesCount.setText(mContext.getString(R.string.likes, video.getLikesCount()));
        holder.bind(video, onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return mVideoList.size();
    }

    private void add(Video item) {
        mVideoList.add(item);
        notifyItemInserted(mVideoList.size()-1);
    }

    public void addAll(List<Video> videos) {
        for (Video video : videos) {
            add(video);
        }
    }

    static public class ViewHolderItem extends RecyclerView.ViewHolder {

        @BindView(R.id.video_name) protected TextView videoName;
        @BindView(R.id.likes_count) protected TextView likesCount;
        @BindView(R.id.video_preview) protected ImageView videoPreview;

        public ViewHolderItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Video item, final OnItemClickListener listener) {
            videoPreview.setOnClickListener(v -> listener.onClick(item));
        }
    }
}
