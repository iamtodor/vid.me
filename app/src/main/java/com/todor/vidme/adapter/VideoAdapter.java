package com.todor.vidme.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devbrackets.android.exomedia.EMVideoView;
import com.todor.vidme.R;
import com.todor.vidme.model.Video;
import com.todor.vidme.model.VideoResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolderItem> {

    private Context mContext;
    private List<Video> mVideoList;

    public VideoAdapter(Context mContext, VideoResponse object) {
        this.mContext = mContext;
        this.mVideoList = object.getVideos();
    }

    @Override
    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderItem(LayoutInflater.from(parent.getContext()).inflate(R.layout.video_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolderItem holder, int position) {
        Video video = mVideoList.get(position);
        holder.videoView.setVideoURI(Uri.parse(video.getCompleteUrl()));
        holder.videoName.setText(video.getTitle());
        holder.likesCount.setText(mContext.getString(R.string.likes, video.getLikesCount()));
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolderItem holder) {
        super.onViewDetachedFromWindow(holder);
        holder.videoView.setReleaseOnDetachFromWindow(false);
    }

    @Override
    public int getItemCount() {
        return mVideoList.size();
    }

    static public class ViewHolderItem extends RecyclerView.ViewHolder {

        @BindView(R.id.video_name) protected TextView videoName;
        @BindView(R.id.likes_count) protected TextView likesCount;
        @BindView(R.id.video_view) protected EMVideoView videoView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
