package com.todor.vidme.ui.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.devbrackets.android.exomedia.EMVideoView;
import com.todor.vidme.R;
import com.todor.vidme.model.Video;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) protected Toolbar toolbar;
    @BindView(R.id.video_name) protected TextView videoName;
    @BindView(R.id.likes_count) protected TextView likesCount;
    @BindView(R.id.video_view) protected EMVideoView videoView;

    public static final String VIDEO = "video";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_activity);
        ButterKnife.bind(this);

        setupToolbar();

        Video video = (Video) getIntent().getSerializableExtra(VIDEO);

        videoView.setMinimumHeight(video.getHeight());
        videoView.setVideoURI(Uri.parse(video.getCompleteUrl()));
        videoName.setText(video.getTitle());
        likesCount.setText(getString(R.string.likes, video.getLikesCount()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
        }
        return false;
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
