package com.todor.vidme.ui.activity;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.todor.vidme.Prefs;
import com.todor.vidme.R;
import com.todor.vidme.adapter.ViewPagerAdapter;
import com.todor.vidme.ui.fragment.FeaturedFragment;
import com.todor.vidme.ui.fragment.FeedFragment;
import com.todor.vidme.ui.fragment.NewsFragment;
import com.todor.vidme.utils.AbstractTabSelectedListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String ACTION_NEWS_FRAGMENT = "com.todor.vid.me.NewsFragment";
    private static final String ACTION_FEED_FRAGMENT = "com.todor.vid.me.FeedFragment";
    @BindView(R.id.tabs) protected TabLayout mTabLayout;
    @BindView(R.id.view_pager) protected ViewPager mViewPager;
    @BindView(R.id.toolbar) protected Toolbar mToolbar;
    private Prefs mPrefs;
    private Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Vid.me");
        mPrefs = new Prefs(this);

        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);

        if (ACTION_NEWS_FRAGMENT.equals(getIntent().getAction())) {
            openNewsFragment();
        } else if (ACTION_FEED_FRAGMENT.equals(getIntent().getAction())) {
            openFeedFragment();
        }

        setTheme(0, getColorForTab(0));

        mTabLayout.addOnTabSelectedListener(new AbstractTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int colorFrom = ((ColorDrawable) mToolbar.getBackground()).getColor();
                int colorTo = getColorForTab(tab.getPosition());
                setTheme(colorFrom, colorTo);
            }
        });
    }

//        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                invalidateOptionsMenu();
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        this.mMenu = menu;
//        getMenuInflater().inflate(R.menu.logout, menu);
//        if (mViewPager.getCurrentItem() == 0) {
//            menu.findItem(R.id.logout).setVisible(false);
//        } else if (mViewPager.getCurrentItem() == 1) {
//            menu.findItem(R.id.logout).setVisible(false);
//        } else if (mViewPager.getCurrentItem() == 2) {
//            menu.findItem(R.id.logout).setVisible(false);
//        }
//        return super.onCreateOptionsMenu(menu);
//    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FeaturedFragment(), getString(R.string.featured));
        adapter.addFragment(new NewsFragment(), getString(R.string.news));
        adapter.addFragment(new FeedFragment(), getString(R.string.feed));
        viewPager.setAdapter(adapter);
    }


    private void openNewsFragment() {
        openFragment(0);
    }

    private void openFeedFragment() {
        openFragment(1);
    }

    private void openFragment(int fragmentId) {
        new Handler().postDelayed(() -> {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (fragmentId == 0) {
                transaction.replace(R.id.content, new NewsFragment());
            } else if (fragmentId == 1) {
                transaction.replace(R.id.content, new FeedFragment());
            }
        }, 500);
    }

    private void setTheme(int colorFrom, int colorTo) {
        ValueAnimator valueAnimator = ValueAnimator.ofObject(new ArgbEvaluator(),
                colorFrom, colorTo);

        valueAnimator.addUpdateListener(animation -> {
            int color = (int) animation.getAnimatedValue();

            mToolbar.setBackgroundColor(color);
            mTabLayout.setBackgroundColor(color);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                this.getWindow().setStatusBarColor(color);
            }
        });

        valueAnimator.setDuration(250);
        valueAnimator.start();
    }

    private int getColorForTab(int position) {
        if (position == 0) {
            return ContextCompat.getColor(this, R.color.purple);
        } else if (position == 1) {
            return ContextCompat.getColor(this, R.color.red);
        } else if (position == 2) {
            return ContextCompat.getColor(this, R.color.light_blue);
        } else {
            throw new RuntimeException("The tab doesn't exist");
        }
    }

    public void setMenuVisibility(boolean visibility) {
        mMenu.findItem(R.id.logout).setVisible(visibility);
    }

}