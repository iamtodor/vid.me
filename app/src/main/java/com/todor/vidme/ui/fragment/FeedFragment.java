package com.todor.vidme.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.todor.vidme.network.API;
import com.todor.vidme.network.ApiService;
import com.todor.vidme.Prefs;
import com.todor.vidme.R;
import com.todor.vidme.utils.DialogUtils;

import butterknife.BindView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FeedFragment extends BaseFragment {

    @BindView(R.id.content) protected FrameLayout content;
    private Prefs mPrefs;
    private ApiService mApiService;
    private API mApi;

    @Override
    protected int getContentViewId() {
        return R.layout.feed_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPrefs = new Prefs(getContext());
        mApiService = new ApiService();
        mApi = mApiService.getApiService();

        setHasOptionsMenu(true);
        replaceFragment();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(getUserVisibleHint()) {
            if (mPrefs.getUser() != null && !TextUtils.isEmpty(mPrefs.getUser().getToken()))
                inflater.inflate(R.menu.logout, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logout) {
            logout();
            return true;
        }
        return false;
    }

    private void logout() {
        if (mPrefs.getUser() != null && !TextUtils.isEmpty(mPrefs.getUser().getToken())) {
            showLoader();
            mApi.logout(mPrefs.getUser().getToken())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .finallyDo(this::hideLoader)
                    .subscribe(loginResponse -> {
                        if (loginResponse.getStatus()) {
                            DialogUtils.getInfoDialog(getContext(), R.string.log_out_message).show();
                            mPrefs.clearAll();
                            replaceFragment();
                        } else {
                            DialogUtils.getInfoDialog(getContext(), R.string.smth_bad).show();
                        }
                    }, throwable -> {
                        DialogUtils.getInfoDialog(getContext(), R.string.smth_bad).show();
                    });
        } else {
            DialogUtils.getInfoDialog(getContext(), R.string.please_login).show();
        }
    }

    private void replaceFragment() {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (mPrefs.getUser() != null && !TextUtils.isEmpty(mPrefs.getUser().getToken())) {
            transaction.replace(R.id.content, new SubscribedFragment());
        } else {
            transaction.replace(R.id.content, new LoginFragment());
        }
        transaction.commit();
    }

}
