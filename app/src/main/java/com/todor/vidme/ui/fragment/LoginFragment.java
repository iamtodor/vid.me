package com.todor.vidme.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.todor.vidme.network.API;
import com.todor.vidme.network.ApiService;
import com.todor.vidme.network.response.ErrorResponse;
import com.todor.vidme.Prefs;
import com.todor.vidme.R;
import com.todor.vidme.utils.DialogUtils;
import com.todor.vidme.utils.NetworkUtils;
import com.todor.vidme.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit.HttpException;
import retrofit.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginFragment extends BaseFragment {

    @BindView(R.id.username) protected EditText username;
    @BindView(R.id.password) protected EditText password;
    @BindView(R.id.log_in) protected Button login;
    private Prefs mPrefs;
    private ApiService mApiService;
    private API mApi;

    @Override
    protected int getContentViewId() {
        return R.layout.login_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        mPrefs = new Prefs(getContext());
        mApiService = new ApiService();
        mApi = mApiService.getApiService();
    }

    @OnClick(R.id.log_in)
    protected void login() {
        String msg = null;

        if (!NetworkUtils.isOnline(getContext())) {
            msg = getString(R.string.no_internet_connection);
        } else if (TextUtils.isEmpty(username.getText().toString())) {
            msg = getString(R.string.username_empty);
        } else if (!Utils.isValidPassword(password.getText().toString())) {
            msg = getString(R.string.wrong_password);
        }

        if (msg != null) {
            DialogUtils.getInfoDialog(getContext(), msg).show();
            return;
        }

        showLoader();
        mApi.login(username.getText().toString(), password.getText().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .finallyDo(this::hideLoader)
                .subscribe(loginResponse -> {
                    if (loginResponse.getStatus() & loginResponse.getError() == null) {
                        mPrefs.saveUser(loginResponse.getAuth());
                        FragmentTransaction transaction = getActivity()
                                .getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, new SubscribedFragment());
                        transaction.commit();
                    } else {
                        DialogUtils.getInfoDialog(getContext(), R.string.smth_bad).show();
                    }
                }, throwable -> {
                    if (throwable instanceof HttpException) {
                        Response response = ((HttpException) throwable).response();
                        try {
                            String error = response.errorBody().string();
                            ErrorResponse errorResponse = getErrorResponseFromString(error);
                            if (!errorResponse.getStatus()) {
                                showError(errorResponse);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private ErrorResponse getErrorResponseFromString(String error) {
        ErrorResponse errorResponse = new ErrorResponse();
        try {
            JSONObject mainObject = new JSONObject(error);
            boolean status = mainObject.getBoolean("status");
            String code = mainObject.getString("code");

            errorResponse.setStatus(status);
            errorResponse.setCode(code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return errorResponse;
    }

    private void showError(ErrorResponse errorResponse) {
        switch (errorResponse.getCode()) {
            case "method_not_allowed":
                DialogUtils.getInfoDialog(getContext(), getString(R.string.bad_request)).show();
                break;
            case "invalid_email":
                DialogUtils.getInfoDialog(getContext(), getString(R.string.valid_email)).show();
                break;
            case "invalid_username":
                DialogUtils.getInfoDialog(getContext(), getString(R.string.valid_username)).show();
                break;
            case "invalid_password":
                DialogUtils.getInfoDialog(getContext(), getString(R.string.valid_password)).show();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mPrefs.getUser() != null && !TextUtils.isEmpty(mPrefs.getUser().getToken()))
            inflater.inflate(R.menu.logout, menu);
    }

}
