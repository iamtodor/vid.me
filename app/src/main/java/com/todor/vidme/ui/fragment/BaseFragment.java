package com.todor.vidme.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.todor.vidme.network.API;
import com.todor.vidme.network.ApiService;
import com.todor.vidme.Prefs;
import com.todor.vidme.utils.DialogUtils;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {

    protected final String TAG = this.getClass().getSimpleName();
    protected API api;
    protected Prefs prefs;

    protected abstract int getContentViewId();

    private Unbinder unbinder;
    private AlertDialog loader;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getContentViewId(), container, false);
        unbinder = ButterKnife.bind(this, view);
        ApiService apiService = new ApiService();
        api = apiService.getApiService();
        prefs = new Prefs(getContext());
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    protected void snackbar(View view, String msg) {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();
    }

    protected void snackbar(View view, @StringRes int msg) {
        snackbar(view, getString(msg));
    }

    protected void showLoader() {
        if (loader == null) {
            loader = DialogUtils.getLoadingDialog(getActivity()).create();
        }
        loader.show();
    }

    protected void hideLoader() {
        if (loader != null) {
            loader.hide();
        }
    }

}
