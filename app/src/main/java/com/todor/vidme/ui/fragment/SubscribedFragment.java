package com.todor.vidme.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.todor.vidme.R;
import com.todor.vidme.adapter.VideoAdapterNew;
import com.todor.vidme.interfaces.OnItemClickListener;
import com.todor.vidme.model.Video;
import com.todor.vidme.ui.activity.VideoActivity;
import com.todor.vidme.utils.DialogUtils;
import com.todor.vidme.utils.NetworkUtils;

import butterknife.BindView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SubscribedFragment extends BaseRVFragment implements OnItemClickListener {

    @BindView(R.id.recycler_view) protected RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout) protected SwipeRefreshLayout swipeRefreshLayout;
    private VideoAdapterNew videoAdapter;

    @Override
    protected int getContentViewId() {
        return R.layout.subscribed_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupRecyclerView(recyclerView);

        if (!NetworkUtils.isOnline(getContext())) {
            DialogUtils.getInfoDialog(getContext(), R.string.no_internet_connection).show();
            return;
        }

        swipeRefreshLayout.setOnRefreshListener(this::loadData);

        showLoader();
        loadData();
    }

    @Override
    public void onClick(Video video) {
        Intent intent = new Intent(getContext(), VideoActivity.class);
        intent.putExtra(VideoActivity.VIDEO, video);
        startActivity(intent);
    }

    @Override
    protected void loadData() {
        api.getFollowedVideo(prefs.getUser().getToken(), 10)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .finallyDo(this::hideLoader)
                .subscribe(videoItem -> {
                    recyclerView.setAdapter(null);
                    videoAdapter = new VideoAdapterNew(getActivity(), videoItem, this);
                    recyclerView.setAdapter(videoAdapter);
                }, throwable -> {
                    DialogUtils.getInfoDialog(getContext(), R.string.smth_bad).show();
                });
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void loadMoreItems() {
        mIsLoading = true;
        mOffset += 10;
        api.getNextFollowedVideo(prefs.getUser().getToken(), 10, mOffset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .finallyDo(() -> mIsLoading = false)
                .subscribe(videoItem -> {
                    videoAdapter.addAll(videoItem.getVideos());
                }, throwable -> {
                    DialogUtils.getInfoDialog(getContext(), R.string.smth_bad).show();
                });
    }
}
