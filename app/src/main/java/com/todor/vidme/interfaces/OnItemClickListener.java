package com.todor.vidme.interfaces;

import com.todor.vidme.model.Video;

public interface OnItemClickListener {
    void onClick(Video video);
}
