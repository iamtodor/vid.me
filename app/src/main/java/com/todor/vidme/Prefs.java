package com.todor.vidme;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.todor.vidme.model.Auth;

public class Prefs {

    private static final String USER = "user";
    private static final String USER_ID = "user_id";
    private static final String TOKEN = "token";
    private static final String TAG = Prefs.class.getSimpleName();

    private final SharedPreferences preferences;
    private Gson gson;
    private Auth auth;

    public Prefs(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.gson = new Gson();
    }

    public void saveUser(Auth auth) {
        this.auth = null;
        preferences.edit().putString(USER, gson.toJson(auth)).apply();
    }

    public Auth getUser() {
        String userJson = preferences.getString(USER, "");
        if (!TextUtils.isEmpty(userJson)) {
            auth = gson.fromJson(userJson, Auth.class);
            Log.d(TAG, "getUser: " + auth.getToken());
        }
        return auth;
    }

    public void clearAll() {
        this.auth = null;
        preferences.edit().clear().apply();
    }

}
