package com.todor.vidme.model;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("status")
    private Boolean status;

    @SerializedName("auth")
    private Auth auth;

    @SerializedName("error")
    private String error;

    @SerializedName("code")
    private String code;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "status=" + status +
                ", auth=" + auth +
                ", error='" + error + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
