package com.todor.vidme.utils;

import android.text.TextUtils;

public final class Utils {

    public static boolean isValidPassword(String password) {
        return !TextUtils.isEmpty(password) && password.length() > 3;
    }

}
