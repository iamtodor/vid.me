package com.todor.vidme.utils;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.todor.vidme.R;

public class DialogUtils {

    public static AlertDialog.Builder getInfoDialog(Context context, String msg) {
        return new AlertDialog.Builder(context)
                .setMessage(msg)
                .setPositiveButton(R.string.ok, null);
    }

    public static AlertDialog.Builder getInfoDialog(Context context, @StringRes int msg) {
        return getInfoDialog(context, context.getString(msg));
    }

    public static AlertDialog.Builder getLoadingDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_loader_layout, null, false);
        return new AlertDialog.Builder(context, R.style.TransparentAlertDialog)
                .setView(view)
                .setCancelable(false);
    }

}
